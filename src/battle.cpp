/**
* battle.cpp
*
* Battle - 101 FLOSS games
*
* 2022 Mohammad Rasmi Khashashneh
*
* MIT license
*
* Permission is hereby granted, free of charge, to any person obtaining a
* copy of this software and associated documentation files (the "Software"),
* to deal in the Software without restriction, including without limitation
* the rights to use, copy, modify, merge, publish, distribute, sublicense,
* and/or sell copies of the Software, and to permit persons to whom the
* Software is furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included
* in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT, IN NO EVENT SHALL
* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
* DEALINGS IN THE SOFTWARE.
*/

#include "battle.hpp"
#include <gameengine_sdl.hpp>
#include <iostream>

namespace battle {
	Battle::Battle() {}

	Battle::~Battle() {}

	int Battle::start() {
		gameengine::WindowProperties properties{"101 FLOSS GAMES: BATTLE", false, 192, 160};
		gameengine::GameEngineSDL engine = gameengine::GameEngineSDL(properties);

		std::unique_ptr<BattleScene> scene = initBattleScene();
		if(!scene) {
			std::cerr << "Error initializing battle  scene" << std::endl;
			return 1;
		}
		engine.setScene(scene.get());
		engine.run();

		return 0;
	}

	std::unique_ptr<BattleScene> Battle::initBattleScene() const {
		auto scene = std::make_unique<BattleScene>();
		//TODO: validate
		return scene;
	}

	int battle() {
		Battle battle = Battle();
		return battle.start();
	}
}
