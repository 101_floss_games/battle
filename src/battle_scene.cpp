/**
* battle_scene.cpp
*
* Battle - 101 FLOSS games
*
* 2022 Mohammad Rasmi Khashashneh
*
* MIT license
*
* Permission is hereby granted, free of charge, to any person obtaining a
* copy of this software and associated documentation files (the "Software"),
* to deal in the Software without restriction, including without limitation
* the rights to use, copy, modify, merge, publish, distribute, sublicense,
* and/or sell copies of the Software, and to permit persons to whom the
* Software is furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included
* in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT, IN NO EVENT SHALL
* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
* DEALINGS IN THE SOFTWARE.
*/

#include <battle_scene.hpp>
#include <iostream>

namespace battle {

	BattleScene::BattleScene() {}

	BattleScene::~BattleScene() {}

	void BattleScene::sceneInit() {
		std::cout << "BattleScene::sceneInit" << std::endl;
		gameengine::Background *background = createBackground();
		add(background);
	}

	void BattleScene::sceneProcess(const float deltaTime) {
		//std::cout << "BattleScene::sceneProcess: deltaTime = " << deltaTime << std::endl;
	}

	void BattleScene::sceneInput(const gameengine::InputEvent inputEvent) {
		switch(inputEvent) {
			case gameengine::InputEvent::INPUT_QUIT:
				std::cout << "Input Quit" << std::endl;
				break;
			case gameengine::InputEvent::INPUT_LEFT_PRESS:
				std::cout << "Input Left Press" << std::endl;
				break;
			case gameengine::InputEvent::INPUT_RIGHT_PRESS:
				std::cout << "Input Right Press" << std::endl;
				break;
			case gameengine::InputEvent::INPUT_UP_PRESS:
				std::cout << "Input Up Press" << std::endl;
				break;
			case gameengine::InputEvent::INPUT_DOWN_PRESS:
				std::cout << "Input Downd Press" << std::endl;
				break;
			case gameengine::InputEvent::INPUT_A_PRESS:
				std::cout << "Input A Press" << std::endl;
				break;
			case gameengine::InputEvent::INPUT_B_PRESS:
				std::cout << "Input B Press" << std::endl;
				break;
			case gameengine::InputEvent::INPUT_X_PRESS:
				std::cout << "Input X Press" << std::endl;
				break;
			case gameengine::InputEvent::INPUT_Y_PRESS:
				std::cout << "Input Y Press" << std::endl;
				break;
			case gameengine::InputEvent::INPUT_L_PRESS:
				std::cout << "Input L Press" << std::endl;
				break;
			case gameengine::InputEvent::INPUT_R_PRESS:
				std::cout << "Input R Press" << std::endl;
				break;
			case gameengine::InputEvent::INPUT_START_PRESS:
				std::cout << "Input Start Press" << std::endl;
				break;
			case gameengine::InputEvent::INPUT_SELECT_PRESS:
				std::cout << "Input Select Press" << std::endl;
				break;
		}
	}

	gameengine::Background* BattleScene::createBackground() {
		background = std::make_unique<gameengine::Background>(backgroundName);
		texture = std::make_unique<gameengine::Texture>(backgroundImagePath);
		background->setTexture(texture.get());
		//TODO: validate
		return background.get();
	}
}
