/**
* scene1.hpp
*
* Battle - 101 FLOSS games
*
* 2022 Mohammad Rasmi Khashashneh
*
* MIT license
*
* Permission is hereby granted, free of charge, to any person obtaining a
* copy of this software and associated documentation files (the "Software"),
* to deal in the Software without restriction, including without limitation
* the rights to use, copy, modify, merge, publish, distribute, sublicense,
* and/or sell copies of the Software, and to permit persons to whom the
* Software is furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included
* in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT, IN NO EVENT SHALL
* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
* DEALINGS IN THE SOFTWARE.
*/

#pragma once
#include <gameengine.hpp>
//TODO: move headers into 1
#include "background.hpp"
#include <string>
#include <memory>

namespace battle {

	class BattleScene : public gameengine::Scene {
		private:
			//TODO: move to a configuration file
			const std::string backgroundImagePath = "assets/scene1/bg.png";
			const std::string backgroundName = "battle_bg";
			std::unique_ptr<gameengine::Background> background;
			std::unique_ptr<gameengine::Texture> texture;

			void sceneInit() override;
			void sceneProcess(const float deltaTime) override;
			void sceneInput(const gameengine::InputEvent inputEvent) override;
			gameengine::Background* createBackground();

		public:
			BattleScene();
			~BattleScene() override;
	};
}
